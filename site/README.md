You can find my site at the adress: 
maxfrebourg.alwaysdata.net

There's already an admin and a user in the database, here are their login credentials:
for the user:
    user@gmail.com
    user

for the admin:
    admin@gmail.com
    admin

You can find the screenshots that were required in the file I've returned.

I didn't make the index page dynamic, instead, I've created a time.php page that is dynamic 
according to time.