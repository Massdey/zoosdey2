<?php include"includes/header.php";
global $db;?>

<!DOCTYPE html>
    <html>
    <?php 
    if (isset($_SESSION['email'])){ ?>
      <head>
        <meta charset="utf-8">
        <title>Test page</title>
        <link rel="icon" href="img/zoo.jpg">
        <script type="text/javascript" src="js/function.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/css2.css">
      </head>

      <body>
      <br/>
      <form>  
        <input type = "button" onclick = "bonjour()" value = "Console Hello">   
      </form>
      
      <h1><p style="text-align:center">Zoosdey</p></h1> 
      <h4><p style="text-align:center"> Connected as: <?php echo $_SESSION['nick'];?></p>
      <div class="container">
        <div class="inside">
          <a class="image" href="africapres.php">
          <p></p>
            <img src="img/afrique.jpg" alt="Africa's geography" width="100%" height="100%" class="zoom" id="img1">
          </a><p></p>
          <span><h4>Description:</h4>   
          here's the " land of Africa " sector, you will find typical<br/>
          animals from Africa : lions, elephants, girafes, zebras,..<br/>
          there are in total 17 animals.</span>
        </div>
        <div class="inside">
          <a class="image" href="asiapres.php">
          <img src="img/asie.jpg" alt="Asia's geography" width="100%" height="100%" class="zoom" id="img2">
          </a>
          <span><h4>Description:</h4>
          here's the " land of Asia " sector, you will find typical animals from Asia :<br/>
          there are in total 19 animals.</span>
        </div>

        <div class="inside">
          <p></p>
          <a class="image" href="southampres.php">
          <img src="img/sudam.jpg" alt="South America's geography" width="100%" height="100%" class="zoom" id="img3">
          </a>
          <span><h4>Description:</h4>
          here's the " South America corner " sector, you will find typical animals from South America :<br/>
          there are in total 12 animals.</span>
        </div>

        <div class="inside">
          <a class="image" href="tropicspres.php">
          <img src="img/tropiques.jpg" alt="Tropics photo" width="100%" height="100%" class="zoom" id="img4">
          </a>
          <span><h4>Description:</h4>
          here's the " Tropics " sector, you will find typical animals from Tropics :<br/>
          there are in total 15 animals.</span>
        </div>
      </div>
    </body>
      <?php } else {
        echo("You're not logged in, you may not use this site, you can choose to register or login <br/><br/> <a href='inscription.php'>Register</a> OR <a href='authentification2.php'>Login</a> <br/><br/>");
      } ?>    
</html>