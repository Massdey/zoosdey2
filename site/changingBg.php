<head>
    <title>Changing Background</title>
    <link rel="stylesheet" href="css/changingBg.css">
</head>

<body>
    <section>
        <h2> The background is changing every second </h2>
        <h4> Wanna go back to index? <a href="index.php">click here</a>)</h4>
    </section>
</body>

<script>
        function changeBackground(){

          const images = [
            'url("img/1.jpg")',
            'url("img/2.jpg")',
            'url("img/3.jpg")',
          ]

          const section = document.querySelector('section');
          const bg = images[Math.floor(Math.random() * images.length)];
          section.style.backgroundImage = bg;
        }
       setInterval(changeBackground,1000);
</script>