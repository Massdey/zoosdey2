<link rel="stylesheet" href="css/smallpages.css">
<?php include('includes/database.php');

function register($db){
    if(isset($_POST['inscription']))  // If connection informations are sent
    {
        $current_date = date('Y-m-d');
        extract($_POST);
        try{
            $c = $db -> prepare('SELECT email FROM users WHERE email = :email');     // check if the emails exists in the database
            $c ->execute(['email' => $email]);
            ([
                'email' => $email
            ]);
    
            $c2 = $db -> prepare('SELECT nick FROM users WHERE nick = :nick');     // check if the nickname exists in the database
            $c2 ->execute(['nick' => $nick]);
            ([
                'nick' => $nick
            ]);
    
            $result_email =$c -> rowCount();    // count the number of lines where this email already exists (it can be 0)
            $result_nick = $c2 -> rowCount(); // count the number of lines where this nickname already exists (it can be 0) 
            if ($result_email == 0)
                {
                  if ($result_nick == 0)
                  {
                    $role="user";   // basic role is user
                    $hashpass = password_hash($password, PASSWORD_BCRYPT); // hash the password
                    $s = $db->query("INSERT INTO `users` (`nick`,`email`,`password`,`regis_date`,`role`) VALUES ('$nick','$email','$hashpass','$current_date','$role'); ");
                    echo " Your registration has been taken into account, please go back to index ";?>
                    <br/><a href="index.php"> Index </a> <?php
                  } else {
                    echo('This nickname has already been taken, please use another one');?>
                    <br/><a href="index.php">Index</a> <?php
                  }
                }else{
                    echo('This email has already been taken, please use another one');?>
                    <a href="index.php">Index<?php
                }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    } else {
        echo "The registration form hasn't been filled";
        ?><br/><a href="index.php">Index</a><?php
    }
}

register($db);?>