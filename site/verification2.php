<head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/smallpages.css">
</head>

<?php session_start();
include "includes/database.php";

//$email=$_POST['email'];
//echo("your email is: $email");?><br/><?php
//$pass=$_POST['password'];
//echo("your password is: $pass");
?><br/><br/><?php // here, we notice that the URL does not
//                                  // include the email and password that we've entered in the f
// $utilisateurs =["Lina@gmail.com","passeLina","Edgar@gmail.com","passeEdgar"];

// if($email==$utilisateurs[0] and $pass==$utilisateurs[1]){
//     $_SESSION['email']=$utilisateurs[0];
//     echo('connected with the email ' . $_SESSION["email"]);
// }
// elseif ($email==$utilisateurs[2] and $pass==$utilisateurs[3]){
//     $_SESSION['email']=$utilisateurs[2];
//     echo('Welcome to your account Edgar');
// }
// else{
//     echo('The couple email, password you have entered is not associated to any account');
// }

// the commented part is the first version with edgar and lina  


function testAndDisplay($db){

    if(isset($_POST['connexion']))  // if connection informations were sent 
    {
        extract($_POST);            // extract form inputs

        if(!empty($email) && !empty($password))         // if email and password have been filled in the form
        {
            $q = $db -> prepare ("SELECT * FROM users WHERE email = :email");            // check if the email exists in the database
            $q -> execute (['email' => $email]);
            $result = $q -> fetch();   
            if( $result == true )                        // if the account exists
            {  
                $hashpass = $result['password'];        // get the hashed password in the database
                if(password_verify($password,$hashpass))   
                {
                    $_SESSION['email'] = $email;    // lines 48 to 50 : define session variables
                    $_SESSION['nick'] = $result['nick'];
                    $_SESSION['role'] = $result['role'];
                    $credentials = [$result['nick'],$result['password']];//array filled with credentials
                    $json_data=json_encode($credentials);//encode our array in json

                    if (file_put_contents("login.json", $json_data)){
                    }
                    else { 
                        echo "There's an error with your json file";    // catch the error 
                    }

                    echo " Now connected as " . $result['nick']; 
                    ?><br/><a href="index.php">Index</a><?php
                    return $json_data;

                }// in this part, we have stored the email and the password in session variables
                else
                {   
                    echo "The password you've entered is not linked to that account ";
                    ?><br/><a href="index.php">Index</a><?php
                }
            }else{
            echo "The email you've entered is not matching any existing account ";
            ?><br/><a href="index.php">Index</a><?php
            }
        }else{
        echo "One mandatory field hasn't been filled ";
        ?><br/><a href="index.php">Index</a><?php
        }
    }else{
    echo "The connection form hasn't been filled, please go back to authentification page";
    ?><br/><a href="index.php">Index</a><?php
    }
}


testAndDisplay($db);
?> 

