<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<?php session_start(); ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="formTicket.php">CreateTicket</a>
      </li>
      <?php if(isset($_SESSION['role']) && $_SESSION['role'] =="admin"){?>
      <li class="nav-item">
        <a class="nav-link" href="printTicket2.php">PrintTicket</a>
      </li>
      <?php } ?>
      <?php if(isset($_SESSION['role'])){?>
      <li class="nav-item">
        <a class="nav-link" href="printTicketList.php">TicketList</a>
      </li>
      <?php } ?>
      <li class="nav-item">
        <a class="nav-link" href="time.php">Time</a>
      </li>
      <?php if(isset($_SESSION['role'])){?>
      <li class="nav-item">
        <a class="nav-link" href="wordpress/">Wordpress</a>
      </li>
      <?php } ?>
      <li class="nav-item">
        <a class="nav-link" href="../changingBg.php">Background</a>
      </li>
      <?php if(isset($_SESSION['email'])){?>
      <li class="nav-item">
        <a class="nav-link" href="includes/logout.php">Logout</a>
      </li>
      <?php } ?>
    </ul>
  </div>
</nav>