<?php 
include "includes/header.php";
?>
<head>
	<link rel="stylesheet" href="css/ticket.css">
</head>
<?php

$timeOfTheDay=date("H");

function testAndDisplay($timeOfTheDay){
	if($timeOfTheDay>0 and $timeOfTheDay<=12){//if it's 0 to 12 am
		?>
		It's morning, here's a zebra:<br/><img src="img/zebra.jpg" alt="Zebra" style="width:30%;">
		<?php
	} 
	if($timeOfTheDay > 12 and $timeOfTheDay <=18){//if it's superior to 12pm to 6pm
		?>
		It's afternoon, here's a girafe:<br/><img src="img/girafe.jpg" alt="Girafe" style="width:20%;">
		<?php 
	}  
	if($timeOfTheDay>18 and $timeOfTheDay<=24){//if it's superior to 6pm but under 12pm
		?>
		It's noon, here's a panda:<br/><img src="img/pandan.jpg" alt="Panda" width=40% height=40%>
		<?php 
	}
	}

if (isset($_SESSION['email'])){ 
	testAndDisplay($timeOfTheDay);
} else { 
	?><p style="padding-top:13%;text-align:center; font-family:sans-serif; font-size:6vh; color:white;">LOG IN</p><?php
}
?>