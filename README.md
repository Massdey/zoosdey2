You can find my site at the adress: 
maxfrebourg.alwaysdata.net

There's already an admin and a user in the database, here are their login credentials:
for the user:
    user@gmail.com
    user

for the admin:
    admin@gmail.com
    admin
    
You have to import my sql database, stored in database/zoo.sql for example in phpmyadmin, so that you can access the user and admin, but also the tickets.

You can find the screenshots that were required in the file I've returned.