-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 29, 2022 at 11:52 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datet` date NOT NULL,
  `login` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `prio` varchar(255) NOT NULL,
  `sector` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`id`, `datet`, `login`, `subject`, `description`, `prio`, `sector`, `status`) VALUES
(1, '2022-04-21', 'max', 'fuite', 'grosse fuite', 'haute', 'afrique', 'happened'),
(2, '2022-05-20', 'nick', 'test', 'test de fou', 'Low', 'Africa', 'willhappen'),
(3, '2022-05-20', 'nick2', 'etet', 'test 2', 'Low', 'Asia', 'happened'),
(4, '2022-05-25', 'usertest', 'testuser', 'je suis user jai un probleme ', 'Low', 'Africa', 'willhappen'),
(5, '2022-05-25', 'usertest', 'testuser2', 'user2test', 'Low', 'Asia', 'happened');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `regis_date` date NOT NULL,
  `role` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nick`, `email`, `password`, `regis_date`, `role`) VALUES
(15, 'usertest', 'user@gmail.com', '$2y$10$lvWq2EvuJStBRdzA4i5XyeV.2X81Y/eS2cYTJipYZHmcinjJKyHwK', '2022-05-25', 'user'),
(14, 'Admin', 'admin@gmail.com', '$2y$10$UVDIMZGK5oDe4PtxPbj/UuuPYRZOn9/9Bdhw7ASK6yPjZwsFHY23m', '2022-05-25', 'admin'),
(13, 'Hashtest', 'hashtest@gmail.com', '$2y$10$hdZud5vcVxevXDELha5pruK903QtNUVwQRP6vI.3GYlalqF0zCPMq', '2022-05-21', 'user');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
